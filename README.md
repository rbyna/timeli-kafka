Kafka

This project does the pre processing on the data(xml to csv conversion) and sending to a particular topic record by record.

Currently, to run locally, you need to change the file path in the code.

1. Run - mvn clean install
2. Hit to produce -
 java -jar target/kafka-producer-consumer-1.0-SNAPSHOT.jar producer test1
 where topic = test1
 
 
Services used:
Apache Zookeeper is used for cluster management.


To run on Microsoft Azure :

1. SSH : ssh sshuser@kafka-timelispeedlayer-ssh.azurehdinsight.net
2. Set few parameters : Execute following :

CLUSTERNAME='kafka-timelispeedlayer'
PASSWORD='Clust3rax$'
export KAFKAZKHOSTS=`curl -sS -u admin:$PASSWORD -G https://$CLUSTERNAME.azurehdinsight.net/api/v1/clusters/$CLUSTERNAME/services/ZOOKEEPER/components/ZOOKEEPER_SERVER | jq -r '["\(.host_components[].HostRoles.host_name):2181"] | join(",")' | cut -d',' -f1,2`

export KAFKABROKERS=`curl -sS -u admin:$PASSWORD -G https://$CLUSTERNAME.azurehdinsight.net/api/v1/clusters/$CLUSTERNAME/services/KAFKA/components/KAFKA_BROKER | jq -r '["\(.host_components[].HostRoles.host_name):9092"] | join(",")' | cut -d',' -f1,2`

3. Build the jar in your local and scp the jar 

scp ./target/kafka-producer-consumer-1.0-SNAPSHOT.jar sshuser@kafka-timelispeedlayer-ssh.azurehdinsight.net:kafka-producer-consumer.jar

4. Topic is already created. IF you want to create a new topic :

/usr/hdp/current/kafka-broker/bin/kafka-topics.sh --create --replication-factor 3 --partitions 8 --topic test --zookeeper $KAFKAZKHOSTS

5. To run the jar :

nohup java -jar kafka-producer-consumer.jar producer $KAFKABROKERS test1 &

6. Check the output :

tail -100f nohup.out


