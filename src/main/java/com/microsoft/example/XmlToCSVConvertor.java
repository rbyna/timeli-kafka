package com.microsoft.example;

import org.w3c.dom.Document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XmlToCSVConvertor {

    public static void xmlToCsvConvertor(String stylesheetPath, File xmlSource, String outputCsvPath) throws Exception {
        File stylesheet = new File(stylesheetPath);
//        File xmlSource = new File(xmlSourcePath);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(xmlSource);

        StreamSource stylesource = new StreamSource(stylesheet);
        Transformer transformer = TransformerFactory.newInstance()
                .newTransformer(stylesource);
        Source source = new DOMSource(document);
        Result outputTarget = new StreamResult(new File(outputCsvPath));
        transformer.transform(source, outputTarget);
        System.out.println("Done.");
    }


  /*  public static void main(String[] args) throws Exception {
        xmlToCsvConvertor(XmlToCSVConvertor.class.getClassLoader().getResource("timestamp.xsl").getPath(),
                XmlToCSVConvertor.class.getClassLoader().getResource("data.xml").getPath(),
                XmlToCSVConvertor.class.getClassLoader().getResource("timestamp.csv").getPath());

        String timestamp = Files.readAllLines(Paths.get(XmlToCSVConvertor.class.getClassLoader()
                .getResource("timestamp.csv").toURI())).stream().findFirst().get();
        System.out.println(timestamp);
    }*/

}
