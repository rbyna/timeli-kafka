package com.microsoft.example;

import kafka.cluster.Broker;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.json.JSONObject;
import org.json.XML;

import com.microsoft.azure.datalake.store.ADLException;
import com.microsoft.azure.datalake.store.ADLStoreClient;
import com.microsoft.azure.datalake.store.DirectoryEntry;
import com.microsoft.azure.datalake.store.IfExists;
import com.microsoft.azure.datalake.store.oauth2.AccessTokenProvider;
import com.microsoft.azure.datalake.store.oauth2.ClientCredsTokenProvider;

import static com.microsoft.example.XmlToCSVConvertor.xmlToCsvConvertor;


public class Producer {
    public static void produce(String brokers) throws Exception {

        // Set properties used to configure the producer
        Properties properties = new Properties();
        // Set the brokers (bootstrap servers) - currently hard coded
        properties.setProperty("bootstrap.servers", brokers);
        // Set how to serialize key/value pairs
        properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);
        String filePath = "/Users/ravitejarajabyna/Downloads/XML/data.xml";
        String newPath = "/Users/ravitejarajabyna/Downloads/XML/data.xml";

        sendMessage(producer, new File(filePath));

        while (true) {

            File inFile = new File(filePath);
            long l = inFile.lastModified();

           Thread.sleep(3000);
            File tempFile = new File(filePath);

            if (l != tempFile.lastModified()) {
                sendMessage(producer, tempFile);
            }
        }

    }

    private static void sendMessage(KafkaProducer<String, String> producer, File xmlFile) throws Exception {


        //Convert xml to csv
//        xmlToCsvConvertor("/home/sshuser/style.xsl",
//                xmlFile,
//                "/home/sshuser/inrix.csv");
        xmlToCsvConvertor("/Users/ravitejarajabyna/ra/timeli-kafka/src/main/resources/style.xsl",
                xmlFile,
                "/Users/ravitejarajabyna/ra/timeli-kafka/src/main/resources/inrix.csv");
        System.out.print("STart time : " + new Date(System.currentTimeMillis()));
        xmlToCsvConvertor("/Users/ravitejarajabyna/ra/timeli-kafka/src/main/resources/timestamp.xsl", xmlFile,
                "/Users/ravitejarajabyna/ra/timeli-kafka/src/main/resources/timestamp.csv");


        String timestamp = Files.readAllLines(Paths.get(Producer.class.getClassLoader().getResource("timestamp.csv")
                .toURI())).stream().findFirst().get();
//        String progressAnimation = "|/-\\";

        //read each line and broadcast
       // Stream<String> stream = Files.readAllLines(Paths.get("/home/sshuser/inrix.csv")).stream();
        Stream<String> stream = Files.readAllLines(Paths.get("/Users/ravitejarajabyna/ra/timeli-kafka/src/main/resources/inrix.csv")).stream();

        try {

            stream.forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    long time = System.currentTimeMillis();
                    System.out.println(timestamp);
                    String s1 = timestamp + ',' + s;
                    producer.send(new ProducerRecord<String, String>("test", s1));
                }
            });
        }finally {

            System.out.print("End time : " + new Date(System.currentTimeMillis()));
            stream.close();
        }
    }

    private static boolean newFile(File tempFile, File inFile) throws IOException {
        boolean isTwoEqual = FileUtils.contentEquals(inFile, tempFile);
        return isTwoEqual;
    }

    //To retrieve broker list
    public static String getBrokerList() throws Exception {
        ZooKeeper zk = new ZooKeeper("localhost:2181", 10000, null);
        List<String> brokerList = new ArrayList<String>();

        List<String> ids = zk.getChildren("/brokers/ids", false);
        for (String id : ids) {
            String brokerInfoString = new String(zk.getData("/brokers/ids/" + id, false, null));
            System.out.println(brokerInfoString);
//            Broker broker = Broker.createBroker(Integer.valueOf(id), brokerInfoString);
//            if (broker != null) {
//                brokerList.add();
//            }
        }
        return String.join(",", brokerList);
    }


    public static void main(String[] args) throws Exception {
        String brokerList = getBrokerList();

    }

}
