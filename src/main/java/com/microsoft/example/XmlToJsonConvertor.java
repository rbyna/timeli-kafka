package com.microsoft.example;

import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class XmlToJsonConvertor {

    public static String xmlToJsonConvertor(String path) throws IOException {
        InputStream inputStream = Producer.class.getClassLoader().getResource(path).openStream();
        String file = read(inputStream);
        JSONObject jsondata = XML.toJSONObject(file);
        return jsondata.toString();
    }

    public static String read(InputStream input) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }
}
