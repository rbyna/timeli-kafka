package com.microsoft.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//The codes are taken from:
//	https://github.com/karande/kafka-producer-file/blob/master/src/main/java/KafkaFileProducer.java

public class InrixDataProducer {

	private String topicName;
	private String brokers;
	//public static final String fileName = "/home/apurba/data/Apr17_EBWB.csv"; // The data to read from
	public static final String fileName = "/Users/ravitejarajabyna/Downloads/XML/data.xml";
	private KafkaProducer<String, String> producer;
	private Boolean isAsync;

	public InrixDataProducer(String topic, Boolean isAsync, String brokers) {
		//Properties props = new Properties();
		//Thread.currentThread().setContextClassLoader(null);
		//props.put("bootstrap.servers", "localhost:9092");
		//props.put("client.id", "DemoProducer");
		//props.put("key.serializer", StringSerializer.class.getName());
		////props.put("value.serializer", StringSerializer.class.getName());
		//props.put("value.serializer", StringSerializer.class.getName());
		//producer = new KafkaProducer<>(props);
		
        // Set properties used to configure the producer
        Properties properties = new Properties();
        // Set the brokers (bootstrap servers) - currently hard coded
        properties.setProperty("bootstrap.servers", brokers);
        // Set how to serialize key/value pairs
        properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        
        producer = new KafkaProducer<>(properties);

		// for kafka-0.8
		// props.put("metadata.broker.list", "localhost:9092");
		// props.put("bootstrap.servers", "localhost:9092");
		// props.put("serializer.class", "kafka.serializer.StringEncoder");
		// props.put("key.serializer", StringSerializer.class.getName());
		// props.put("value.serializer", StringSerializer.class.getName());
		// producer = new KafkaProducer<>(props);
		this.topicName = topic;
		this.isAsync = isAsync;
		this.brokers = brokers;
	}

	public void sendMessage(String value) {
		long startTime = System.currentTimeMillis();
		if (isAsync) {

		} else { // send synchronously

			try {

				producer.send(new ProducerRecord<>(topicName, value)).get();

				//System.out.println(value);
			} catch (InterruptedException e) {

				e.printStackTrace();

			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}



	public void produce(String filename) throws FileNotFoundException, Exception {
		long startTime = System.currentTimeMillis();
		System.out.println(new Date());
		InrixDataProducer producer = new InrixDataProducer(topicName, false, "localhost:9092");

		int lineCount = 0;
		File inFile;
		BufferedReader br = null;
		
		inFile = new File(fileName);
		br = new BufferedReader(new FileReader(inFile));
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inFile);
		doc.getDocumentElement().normalize();
		//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList = doc.getElementsByTagName("Segment");
		
		NodeList listforts = doc.getElementsByTagName("SegmentSpeedResults");
		
		Element tselement = (Element)listforts.item(0);
		
		String ts = tselement.getAttribute("timestamp");
		
		String line = null;
		
		int count = 0;
		
		for(int i=0; i < nList.getLength(); i++) {
			Node segmentNode = nList.item(i);
			if(segmentNode.getNodeType() == Node.ELEMENT_NODE){
				Element eElement = (Element) segmentNode;
				String code = eElement.getAttribute("code");
				String type = eElement.getAttribute("type");
				String speed = eElement.getAttribute("speed");
				String average = eElement.getAttribute("average");
				String reference = eElement.getAttribute("reference");
				String score = eElement.getAttribute("score");
				String cvalue = eElement.getAttribute("c-value");
				String travelTimeMinutes = eElement.getAttribute("travelTimeMinutes");
				
				line = ts + "," + code + "," + type + "," + speed + "," + average + "," + 
						reference + "," + score + "," + cvalue + "," + travelTimeMinutes;
				
				this.sendMessage(line);
				
				count++;
			}


		}
		long endTime = System.currentTimeMillis();
		System.out.println(new Date(endTime));
		System.out.println(endTime - startTime);
		

		while (true) {

			try {
				
				File tempFile = new File(fileName);
				boolean isTwoEqual = FileUtils.contentEquals(inFile, tempFile);
				
				if(isTwoEqual)
					continue;
				
				inFile = new File(fileName);

				br = new BufferedReader(new FileReader(inFile));
				dbFactory = DocumentBuilderFactory.newInstance();
				dBuilder = dbFactory.newDocumentBuilder();
				doc = dBuilder.parse(inFile);
				doc.getDocumentElement().normalize();
				//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				nList = doc.getElementsByTagName("Segment");
				
				listforts = doc.getElementsByTagName("SegmentSpeedResults");
				
				tselement = (Element)listforts.item(0);
				
				ts = tselement.getAttribute("timestamp");
				
				line = null;
				
				count = 0;
				
				for(int i=0; i < nList.getLength(); i++) {
					Node segmentNode = nList.item(i);
					if(segmentNode.getNodeType() == Node.ELEMENT_NODE){
						Element eElement = (Element) segmentNode;
						String code = eElement.getAttribute("code");
						String type = eElement.getAttribute("type");
						String speed = eElement.getAttribute("speed");
						String average = eElement.getAttribute("average");
						String reference = eElement.getAttribute("reference");
						String score = eElement.getAttribute("score");
						String cvalue = eElement.getAttribute("c-value");
						String travelTimeMinutes = eElement.getAttribute("travelTimeMinutes");
						
						line = ts + "," + code + "," + type + "," + speed + "," + average + "," + 
								reference + "," + score + "," + cvalue + "," + travelTimeMinutes;
						
						this.sendMessage(line);
						
						count++;
					}
				}
				/*long endTime = System.currentTimeMillis();
				System.out.println(new Date(endTime));
				System.out.println(endTime - startTime);*/


				
				//while ((line = br.readLine()) != null) {
				//	lineCount++;
				//	producer.sendMessage(lineCount + " ", line);
				//}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {

					br.close();
					


				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		/*
		int lineCount = 0;
		FileInputStream fis;
		BufferedReader br = null;

		try {
			fis = new FileInputStream(fileName);
			br = new BufferedReader(new InputStreamReader(fis));

			String line = null;
			while ((line = br.readLine()) != null) {
				lineCount++;
				this.sendMessage(lineCount + " ", line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

				br.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		*/
	}




	/*public static void main(String[] args) throws FileNotFoundException, Exception {

		InrixDataProducer producer = new InrixDataProducer(topicName, false);

		int lineCount = 0;
		File inFile;
		BufferedReader br = null;

		inFile = new File(fileName);
		br = new BufferedReader(new FileReader(inFile));
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inFile);
		doc.getDocumentElement().normalize();
		//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		NodeList listforts = doc.getElementsByTagName("SegmentSpeedResults");

		Element tselement = (Element)listforts.item(0);

		String ts = tselement.getAttribute("timestamp");


		NodeList nList = doc.getElementsByTagName("Segment");

		String line = null;

		//Map<String, String> segmentData = new HashMap<>();

		int count = 0;

		for(int i=0; i < nList.getLength(); i++) {
			Node segmentNode = nList.item(i);
			if(segmentNode.getNodeType() == Node.ELEMENT_NODE){
				Element eElement = (Element) segmentNode;
				//segmentData.put("code", eElement.getAttribute("code"));
				String code = eElement.getAttribute("code");
				//segmentData.put("type", eElement.getAttribute("type"));
				String type = eElement.getAttribute("type");
				//segmentData.put("speed", eElement.getAttribute("speed"));
				String speed = eElement.getAttribute("speed");
				//segmentData.put("average", eElement.getAttribute("average"));
				String average = eElement.getAttribute("average");
				//segmentData.put("reference", eElement.getAttribute("reference"));
				String reference = eElement.getAttribute("reference");
				//segmentData.put("score", eElement.getAttribute("score"));
				String score = eElement.getAttribute("score");
				//segmentData.put("cvalue", eElement.getAttribute("c-value"));
				String cvalue = eElement.getAttribute("c-value");
				//segmentData.put("travelTimeMinutes", eElement.getAttribute("travelTimeMinutes"));
				String travelTimeMinutes = eElement.getAttribute("travelTimeMinutes");



				line = ts + "," + code + "," + type + "," + speed + "," + average + "," +
						reference + "," + score + "," + cvalue + "," + travelTimeMinutes;

				producer.sendMessage(line);

				count++;
			}
		}


		while (true) {

			try {

				File tempFile = new File(fileName);
				boolean isTwoEqual = FileUtils.contentEquals(inFile, tempFile);

				if(isTwoEqual)
					continue;

				inFile = new File(fileName);

				br = new BufferedReader(new FileReader(inFile));
				dbFactory = DocumentBuilderFactory.newInstance();
				dBuilder = dbFactory.newDocumentBuilder();
				doc = dBuilder.parse(inFile);
				doc.getDocumentElement().normalize();
				//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				nList = doc.getElementsByTagName("Segment");

				line = null;

				count = 0;

				for(int i=0; i < nList.getLength(); i++) {
					Node segmentNode = nList.item(i);
					if(segmentNode.getNodeType() == Node.ELEMENT_NODE){
						Element eElement = (Element) segmentNode;
						String code = eElement.getAttribute("code");
						String type = eElement.getAttribute("type");
						String speed = eElement.getAttribute("speed");
						String average = eElement.getAttribute("average");
						String reference = eElement.getAttribute("reference");
						String score = eElement.getAttribute("score");
						String cvalue = eElement.getAttribute("c-value");
						String travelTimeMinutes = eElement.getAttribute("travelTimeMinutes");

						line = code + "," + type + "," + speed + "," + average + "," +
								reference + "," + score + "," + cvalue + "," + travelTimeMinutes;

						producer.sendMessage( line);

						count++;
					}
				}

				//while ((line = br.readLine()) != null) {
				//	lineCount++;
				//	producer.sendMessage(lineCount + " ", line);
				//}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {

					br.close();



				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// producer.produce(fileName);

	}
*/
}
